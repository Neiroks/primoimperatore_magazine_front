
/**
 * Форматирования числа в денежный формат
 * @param {Number} number - число для форматирования
 * @param {Number} decimals - знаков после запятой
 * @param {String} decPoint - разделитель для float
 * @param {String} thousandsSep - разделить разрядов
 * @returns {string} число в денежном формате
 */
 function numberFormat(number, decimals = 0, decPoint = '', thousandsSep = ' ') {
   let i,
       j,
       kw,
       kd,
       km;

   i = parseInt(number = (+number || 0).toFixed(decimals)) + '';
   if ((j = i.length) > 3) {
       j = j % 3;
   } else {
       j = 0;
   }
   km = (j ? i.substr(0, j) + thousandsSep : '');
   kw = i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousandsSep);
   kd = (decimals ? decPoint + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : '');
   return km + kw + kd;
}

export {numberFormat};