import * as $ from 'jquery';
import { initFormWithValidate } from "../form";
import {showLoader, hideLoader} from "../loader";

export default class InitLoginForm{
   constructor(){
      this.$form = $('#login_form');
      this.$formMessageBlock = this.$form.find('.form-message');
      this.$showPass = $('#show_pass');
      
      if (!this.$form.length) return false;
      
      this.successForm = this.successForm.bind(this);
      this.errorForm = this.errorForm.bind(this);
      this.clearForm = this.clearForm.bind(this);

      this.init();
   }

   init(){
      initFormWithValidate(this.$form);

      this.$showPass.on('click', (e)=>{
         e.preventDefault();
         $('#show_pass').toggleClass('icon-eye');

         if($('#login_password').attr('type') === 'password'){
            $('#login_password').attr('type','text');
         }else if($('#login_password').attr('type') === 'text'){
            $('#login_password').attr('type','password');
         }
      })

      this.submit(this.successForm, this.errorForm);
   }

   submit(successFn, errorFn) {
      this.$form.on('submit',  (e)=> {
         e.preventDefault();

         showLoader($('#login_form'),$('#login_loader'))

         let $formData = {};

         $(this).find('input, textarea, select').each(function () {
            $formData[this.name] = $(this).val();
         });

         let data = {
            sub: 100,
            cc: 100,
            login: $formData.login_form_name,
            password: $formData.login_form_password
         };

         $.ajax({
            url: $('#login_form').attr('action'),
            type: 'POST',
            dataType: 'text',
            data: data,
            success:  (res) => {
               successFn()
            },
            error:  (res) => {

               /* setTimeout для примера прелоадера */
               setTimeout(() => {
                  errorFn(res);
               }, 3000);
            },
            timeout: 30000
         });

      })
   }

   successForm() {
      this.clearForm();

      this.$formMessageBlock.find('.message-title').text('Отлично!');
      this.$formMessageBlock.find('.message-subtitle').text('Ваше собщение успешно отправлено');

      this.$form.addClass('form-hide');
   }

   errorForm(res) {

      /* res для выведения ошибки в message-title и message-subtitle*/
      this.clearForm();

      hideLoader($('#login_loader'));
      $('.login-wrapper').addClass('error');

      $('.form-message').removeClass('hide');
      $('.form-message').find('.message-title').text('Ошибка');
      $('.form-message').find('.message-subtitle').text('Попробуйте еще раз');

      $('.form-message').find('#close_error').on('click', function(){
         $('.form-message').addClass('hide');
         $('.login-wrapper').removeClass('error');
         $('#login_form').removeClass('hide');
      })
   }

   clearForm() {
      this.$form[0].reset();
      this.$form.find('.field').removeClass('success').addClass('empty');
   }
}