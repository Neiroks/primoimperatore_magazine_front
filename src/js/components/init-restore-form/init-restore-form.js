import * as $ from 'jquery';
import { initFormWithValidate } from "../form";
import {showLoader, hideLoader} from "../loader";

export default class InitRestoreForm{
   constructor(){
      this.$form = $('#restore_form');
      this.$formMessageBlock = this.$form.find('.form-message');
      
      if (!this.$form.length) return false;
      
      this.successForm = this.successForm.bind(this);
      this.errorForm = this.errorForm.bind(this);
      this.clearForm = this.clearForm.bind(this);

      this.init();
   }

   init(){
      initFormWithValidate(this.$form);
      this.submit(this.successForm, this.errorForm);
   }

   submit(successFn, errorFn) {
      this.$form.on('submit',  (e)=> {
         e.preventDefault();

         let $formData = {};

         $(this).find('input, textarea, select').each( ()=> {
            $formData[this.name] = $(this).val();
         });

         let data = {
            sub: 100,
            cc: 100,
            login: $formData.restore_form_name
         };

         showLoader($('#restore_form'),$('#restore_loader'));

         console.log('Данные формы:',data)

         $.ajax({
            url: $('#restore_form').attr('action'),
            type: 'POST',
            dataType: 'text',
            data: data,
            success:  (res) => {
               successFn()
            },
            error:  (res) => {

               /* setTimeout для примера прелоадера */
               setTimeout(() => {
                  errorFn(res);
               }, 3000);
            },
            timeout: 30000
         });
      })
   }

   successForm() {
      this.clearForm();

      $('#restore_loader').addClass('hide');
      $('.restore-wrapper').addClass('success');

      $('.form-message-success').removeClass('hide');
      $('.form-message-success').find('.message-title').text('Пароль отправлен');
      $('.form-message-success').find('.message-subtitle').text('Проверьте вашу почту');
      $('.form-message-success').find('#close_error').text('Войти');

      $('.form-message-success').find('#close_error')
      .on('click', () => {
         $('.form-message-success').addClass('hide');
         $('.restore-wrapper').removeClass('success');
         $('#restore_form').removeClass('hide');
      })
   }

   errorForm(res) {

      /* res для выведения ошибки в message-title и message-subtitle*/
      this.clearForm();

      hideLoader($('#restore_loader'));

      $('.form-message').removeClass('hide');
      $('.form-message').find('.message-title').text('Ошибка');
      $('.form-message').find('.message-subtitle').text('Попробуйте еще раз');
      $('.form-message').find('#close_error').text('Закрыть');

      $('.form-message').find('#close_error')
      .on('click', () => {
         $('.form-message').addClass('hide');
         $('.restore-wrapper').removeClass('error');
         $('#restore_form').removeClass('hide');
      })
   }

   clearForm() {
      this.$form[0].reset();
      this.$form.find('.field').removeClass('success').addClass('empty');
   }
}