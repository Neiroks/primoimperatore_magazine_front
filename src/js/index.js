import * as $ from 'jquery';
import {
   initMaskedInput,
   initPlaceholders
} from './components/form';
import { InitLoginForm } from "./components/init-login-form";
import {InitRestoreForm} from "./components/init-restore-form";
import { Catalogue } from './components/catalogue';
import { Selects } from './components/form';


let switcher = $('body').find('.switcher');

$(function(){
   initScripts();
});

function initScripts() {

   updateViewportHeight();

   $(window).on('resize', () => {
      updateViewportHeight();
  });

   function isDesktop() {
      return (window.innerWidth > 1000);
   }

   if(window.location.pathname.split( '/' )[3] == 'login.html' || window.location.pathname.split( '/' )[3] == 'restore.html') {
      $('.header-ui__cart').addClass('hide');
      $('.header-ui__user').addClass('hide');
   }

   //Перенести на страницу с карточками!
   if(switcher.length){
      switcher.on('click','label', (e)=>{
         const $this = e.currentTarget;
         $this.classList.toggle('active');
        
         return false;
      })
   }

   //select для языков десктоп
   $('.header-ui__language').on('click', ()=>{
      if($('.languages').hasClass('hide')){
         showCustomSelect('desktop');
      }else{
        hideCustomSelect('desktop');
      }

      $('.languages').on('mouseleave', () => {
         hideCustomSelect('desktop');
      })
   })

   $('.languages').on('click','span',(e)=>{
      e.preventDefault();
      const $this = e.currentTarget;
      $('#laguage_select').html(`${$(e.currentTarget).html()}`);
      $('#laguage_select').attr('value',`${$(e.currentTarget).attr('value')}`);
      hideCustomSelect('desktop');
   })

    //select для языков мобил
   if(!isDesktop()){
      $('#laguage_select_mobile').on('click', ()=>{
         if($('.languages').hasClass('hide')){
            showCustomSelect('mobile');
         }else {
           hideCustomSelect('mobile');
         }
      })
      
      $('.languages').on('click','span',(e)=>{
         e.preventDefault();
         const $this = e.currentTarget;
         $('#laguage_select_mobile').html(`${$(e.currentTarget).html()}`);
         $('#laguage_select_mobile').attr('value',`${$(e.currentTarget).attr('value')}`);
         hideCustomSelect('mobile');
      })
   
      /* $(document).on('click', (e) => {
         if ($('.languages').has(e.target).length === 0 
         && !$('.languages').hasClass('hide')) {
            hideCustomSelect('mobile');
         };
      }); */
   }

   //select для профиля десктоп
   $('#header_email').on('click', ()=>{
      if($('.user-select').hasClass('hide')){
         $('.user-select').removeClass('hide');
      }else{
         $('.user-select').addClass('hide');
      }

      $('.user-select').on('mouseleave', () => {
         $('.user-select').addClass('hide');
      })
   })
   
   //seelct для профиля мобильная
   if(!isDesktop()){
      $('#header_profile_icon').on('click', ()=>{
         if($('.user-select').hasClass('hide')){
            $('.user-select').removeClass('hide');
         }else{
            $('.user-select').addClass('hide');
         }
      })
   }


   //инициализация логин формы
   new InitLoginForm();

   //инициализация формы восст пароля
   new InitRestoreForm();

    // Инициализация плейсхолдеров и масок
    initMaskedInput();
    initPlaceholders();

    //каталог
    new Catalogue();
}

const updateViewportHeight = () => {
   if ($(document).width() < 1000) {
       document.documentElement.style.setProperty('--viewport-height', `${window.innerHeight - 64}px`);
   } else {
       document.documentElement.style.setProperty('--viewport-height', `${window.innerHeight - 96}px`);
   }
};

const showCustomSelect = (param) => {

   $('.languages').removeClass('hide');
   if(param === 'desktop'){
      $('.header-ui__language').find('.icon-arrow-down-header')
      .css('transform','rotate(180deg)');

   }else if(param === 'mobile'){
      $('.header-mobile-content__language-block').find('.icon-arrow-down-header')
      .css('transform','rotate(180deg)');
   }
}

const hideCustomSelect = (param) =>{
  
   $('.languages').addClass('hide');
   if(param === 'desktop'){
      $('.header-ui__language').find('.icon-arrow-down-header')
      .css('transform','rotate(0)');

   }else if(param === 'mobile'){
      $('.header-mobile-content__language-block').find('.icon-arrow-down-header')
      .css('transform','rotate(0)');
   }
}